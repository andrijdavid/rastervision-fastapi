import sys
from fastapi import FastAPI, File, UploadFile
from rastervision.pipeline import rv_config
from rastervision.core.predictor import Predictor
from config import *
from typing import Optional
from pydantic import BaseModel
from fastapi.responses import FileResponse
from pathlib import Path
import tempfile
import aiofiles
import os

version = f"{sys.version_info.major}.{sys.version_info.minor}"
app = FastAPI(title="Raster Vision")

base_path = Path(os.path.dirname(os.path.realpath(__file__)))
predictor = Predictor(MODEL_BUNDLE_URI, TMP_DIR, update_stats=False, channel_order=CHANNEL_ORDER)

async def predict(image: UploadFile):
    await image.seek(0)
    byte = await image.read()

    temp_path = Path(TMP_DIR) / image.filename
    temp = await aiofiles.open(str(temp_path), mode='w+b')
    await temp.write(byte)
    await temp.close()

    predictor.predict([str(temp_path)], "predictions", vector_label_uri="vector_label")
    
    await aiofiles.os.remove(str(temp_path))

    pred_path = base_path / "predictions" / "labels.tif"
    vect_path = base_path / "vector_label" / "0-polygons.json"
    return pred_path, vect_path

@app.get("/ping")
async def ping():
    return "pong"

@app.post("/predict/labels")
async def predict_label(image: UploadFile = File(...)):
    pred_path, vect_path = await predict(image)
    return FileResponse(str(pred_path), filename="labels.tif")

@app.post("/predict/geojson")
async def predict_json(image: UploadFile = File(...)):
    pred_path, vect_path = await predict(image)
    return FileResponse(str(vect_path), filename="geo.geojson")