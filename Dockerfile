ARG TAG

FROM quay.io/azavea/raster-vision:$TAG

RUN pip install "uvicorn[standard]" gunicorn fastapi aiofiles  python-multipart aiohttp
COPY ./start.sh /start.sh
RUN chmod +x /start.sh

COPY ./gunicorn_conf.py /gunicorn_conf.py

COPY ./start-reload.sh /start-reload.sh
RUN chmod +x /start-reload.sh


COPY ./app /opt/src/
# WORKDIR /app/

# ENV PYTHONPATH=${PYTHONPATH}:/app
# ENV WORKERS_PER_CORE=1
# ENV LOG_LEVEL=debug
# ENV MAX_WORKERS=1
ENV TIMEOUT=700
EXPOSE 80
CMD ["/start.sh"]