# RasterVsiion Docker Images with FastApi

This image is used for deployment of RasterVision model bundle as Rest API.

## How to build

To build and push: `make`
To build : `make build RASTERVER=pytorch-0.12`
To Push : `make push`

The `RASTERVER` handle the tag of the base image (rastervision ). You can find them here https://quay.io/repository/azavea/raster-vision?tab=tags.

To build and push a particular version: `make RASTERVER=pytorch-latest`

The variables:
* IMAGENAME: The docker image name.
* REPO: The repository to push the image to.

This image is published at quay.io/andrijdavid/rastervision-fastapi

## How to use 

Put the model-bundle.zip inside the folder model-bundle.

`docker run -p 80:80 -v ${PWD}/model-bundle/model-bundle.zip:/model-bundle.zip quay.io/andrijdavid/rastervision-fastapi:pytorch-0.13`

Once running, you can open your browser `localhost/docs` to see all the available api calls.
