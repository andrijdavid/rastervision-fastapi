#Dockerfile vars
RASTERVER=pytorch-0.12

#vars
IMAGENAME=rastervision-fastapi
REPO=quay.io/andrijdavid
IMAGEFULLNAME=${REPO}/${IMAGENAME}:${RASTERVER}

.PHONY: help build push all

help:
	    @echo "Makefile arguments:"
	    @echo ""
	    @echo "RASTERVER - RasterVision Version"
	    @echo ""
	    @echo "Makefile commands:"
	    @echo "build"
	    @echo "push"
	    @echo "all"

.DEFAULT_GOAL := all

build:
	    @docker build --pull --build-arg TAG=${RASTERVER} -t ${IMAGEFULLNAME} .

push:
	    @docker push ${IMAGEFULLNAME}

run:
		@docker run -p 80:80 -v ${PWD}/model-bundle/model-bundle.zip:/model-bundle.zip ${IMAGEFULLNAME}

test: 
		@docker run -it -p 80:80 ${IMAGEFULLNAME} bash
all: build push